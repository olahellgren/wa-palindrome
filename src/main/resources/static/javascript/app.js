//app.js
/*jslint browser: true*/
/*global $, angular*/
(function () {
    "use strict";
    var app = angular.module('palindrome', [
        'ngRoute'
    ]);

    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
            when('/', {
                templateUrl: 'templates/main.html',
                controller: 'MainCtrl'
            }).
            when('/404', {
                templateUrl: 'templates/404.html'
            }).
            otherwise({
                redirectTo: '/404'
            });
        }]);

    app.controller('MotherCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.palindromeText = null;
        $scope.palindromeClasses = "glyphicon-remove palindrome-false";
        $scope.palindromeVerified = null;

        $scope.checkPalindrome = function(palindrome){
            $scope.palindromeText = palindrome;
            /* Check if palindrome */
            if(palindrome.length > 0) {
            $http.get('api/palindrome/' + palindrome)
                   .success(function (data, status, headers, config) {
                       if(JSON.parse(data) == true){
                            $scope.palindromeClasses = "glyphicon-ok palindrome-true";
                            $scope.palindromeVerified = true;
                       } else {
                            $scope.palindromeClasses = "glyphicon-remove palindrome-false";
                            $scope.palindromeVerified = false;
                       }
                       console.log($scope.palindromeVerified);
                   })
                   .error(function (data, status, headers, config) {
                       console.error("Could check api if palindrome");
                   });
            } else {
                $scope.palindromeClasses = "glyphicon-remove palindrome-false";
                $scope.palindromeVerified = null;
            }

        };

    }]);

    app.directive('topNavigation', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/page-header>.html'
        }
    });

    app.directive('bottomFooter', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/bottom-footer.html'
        }
    });



}());
