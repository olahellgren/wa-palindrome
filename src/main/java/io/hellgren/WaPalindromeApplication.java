package io.hellgren;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaPalindromeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WaPalindromeApplication.class, args);
	}
}
