package io.hellgren.util;

/**
 * Util for checking if a {@link String} is a palindrome
 * <p>
 * Wikipedia definition of <strong>palindrome</strong> (http://en.wikipedia.org/wiki/Palindrome)<br />
 * A <strong>palindrome</strong> is a word, phrase, number, or other sequence of characters which reads the same backward or forward.
 * Allowances may be made for adjustments to capital letters, punctuation, and word dividers. Examples in English
 * include "A man, a plan, a canal, Panama!", "Amor, Roma", "race car", "stack cats", "step on no pets", "taco cat",
 * "put it up", "Was it a car or a cat I saw?" and "No 'x' in Nixon".
 * <p>
 * Different kind of palindromes:
 * <ul>
 * <li>Character by Character - supported
 * <li>Word Palindrome
 * <li>Line-unit Palindrome
 * <li>Word-unit Palindrome
 * </ul>
 *
 * @author Ola Hellgren
 */
public final class PalindromeUtil {

    /**
     * Checks if string is palindrome, all special characters will be removed and
     * {@link String} will be converted to lower case when checking if a {@link String} is a palindrome.
     * Eg. "No 'x' in Nixon" will be checked as "noxinnixon".
     *
     * @param s string to check
     * @return true if palindrome
     */
    public static boolean isCharByCharPalindrome(String s){
        char[] chars = s.replaceAll("[^\\p{L}\\p{N}]+", "").toLowerCase().toCharArray();
        int i = 0;
        int j = chars.length - 1;
        while (i < j) {
            if(chars[i] != chars[j]) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}
