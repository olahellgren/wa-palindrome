package io.hellgren.controller;

import io.hellgren.util.PalindromeUtil;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

    @RequestMapping(value = "/palindrome/{string}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public HttpEntity<Boolean> isPalindrome(@PathVariable String string) {
        return new HttpEntity<>(PalindromeUtil.isCharByCharPalindrome(string));
    }

}
