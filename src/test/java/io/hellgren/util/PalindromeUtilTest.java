package io.hellgren.util;

import io.hellgren.util.PalindromeUtil;
import org.junit.Test;

import static org.junit.Assert.*;

public class PalindromeUtilTest {

    public static final String S_IS_NOT_A_PALINDROME = "%s is not a palindrome";
    public static final String S_IS_A_PALINDROME = "%s is a palindrome";

    @Test
    public void testIsPalindrome_knockknock_false() {
        String s = "no palindrome";
        assertFalse(String.format(S_IS_A_PALINDROME, s), PalindromeUtil.isCharByCharPalindrome(s));
    }

    @Test
    public void testIsPalindrome_nixon_true() {
        String s = "No 'x' in Nixon";
        assertTrue(String.format(S_IS_NOT_A_PALINDROME, s), PalindromeUtil.isCharByCharPalindrome(s));
    }

    @Test
    public void testIsPalindrome_12321_true() {
        String s = "12321";
        assertTrue(String.format(S_IS_NOT_A_PALINDROME, s), PalindromeUtil.isCharByCharPalindrome(s));
    }

    @Test
    public void testIsPalindrome_a_true() {
        String s = "a";
        assertTrue(String.format(S_IS_NOT_A_PALINDROME, s), PalindromeUtil.isCharByCharPalindrome(s));
    }
}